from distutils.core import setup

# TODO We should clean this up

setup(
  name = 'sharedlib',
  packages = ['sharedlib'],
  version = '0.2',
  license='MIT',
  description = 'TYPE YOUR DESCRIPTION HERE',
  author = 'YOUR NAME',
  author_email = 'your.email@domain.com',
  url = 'https://github.com/user/reponame',
  download_url = 'https://github.com/user/reponame/archive/v_01.tar.gz',
  keywords = [],
  install_requires=[],
  classifiers=[
    'Development Status :: 3 - Alpha',
    'Intended Audience :: Developers',
    'Topic :: Software Development :: Build Tools',
    'License :: OSI Approved :: MIT License',
    'Programming Language :: Python :: 3.6',
  ],
)